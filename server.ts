/** Created by Jan de Rijke */
import {createServer, IncomingMessage, ServerResponse} from "http";

"http"
let poort = 3000;

function handleRequest(request:IncomingMessage, response:ServerResponse) {
	console.log(`URL gevraagd: ${request.url}`)
	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end("Goedenavond!");
}

let srv= createServer(handleRequest);
srv.listen(poort);
console.log(`Server gestart op http://localhost:${poort}!'`)


