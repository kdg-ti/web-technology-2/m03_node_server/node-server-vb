import path from "path";
import http, {Server, ServerResponse, IncomingMessage} from "http";
import fs, {Stats} from "fs";

const poort: number = 3000;
const root: string = path.resolve("public");

function handleRequest(request: IncomingMessage, response: ServerResponse): void {
    const file: string = path.join(root, request.url!);
    fs.stat(file, (err: Error | null, stat: Stats) => serveFile(err, stat, file, response));
}

function serveFile(err: Error | null, stat: Stats, file: string, response: ServerResponse): void {
    if (err || !stat.isFile()) {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.write(`Bestand ${file} niet beschikbaar`);
        response.end();
        return;
    }
    response.writeHead(200, {'Content-Type': 'text/html'});
    fs.createReadStream(file).pipe(response);
}

const srv: Server = http.createServer(handleRequest);
srv.listen(poort);
console.log(`Server gestart op http://localhost:${poort}!`);
