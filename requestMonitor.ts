import http, { IncomingMessage, ServerResponse } from "http";

function handleRequest(req: IncomingMessage, res: ServerResponse): void {
    const baseUrl: string = 'http://' + req.headers.host + '/';
    console.log(`Served at: ${baseUrl}`);
    console.log(`Method: ${req.method}`);

    const req_url: URL = new URL(req.url!, baseUrl);
    console.log(`Path: ${req_url.pathname}`);
    console.log(`Parameters: ${req_url.searchParams}`);
    req_url.searchParams.forEach((value: string, key: string) => {
        console.log(`\t${key} = ${value}`);
    });
    res.end("It's all in the logs!");
}

http.createServer(handleRequest).listen(3000);
