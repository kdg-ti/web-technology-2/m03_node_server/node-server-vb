import https from "https";
import {IncomingMessage} from "http";

https.get("https://www.kdg.be/", handleResponse);

function handleResponse(page: IncomingMessage) {
    const code = page?.statusCode;
    if (code && code >= 300) {
        console.log(`HTTP code ${code} voor pagina ${page}.`);
        return;
    }
    page.pipe(process.stdout);
}
