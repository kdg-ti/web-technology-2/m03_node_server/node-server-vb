import http, {IncomingMessage, Server, ServerResponse} from "http";
import fs, {Stats} from "fs";
import path from "path";
import zlib from "zlib";

const poort: number = 3000;
const root: string = path.resolve("public");

function handleRequest(request: IncomingMessage, response: ServerResponse): void {
    const file: string = path.join(root, request.url!);
    fs.stat(file, (err, stat) => serveZip(err, stat, file, response));
}

function serveZip(err: Error | null, stat: Stats, file: string, response: ServerResponse): void {
    if (err || !stat.isFile()) {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write(`Bestand ${file} niet beschikbaar`);
        response.end();
        return;
    }
    response.writeHead(200, {"Content-Type": "text/html", "Content-Encoding": "gzip"});
    fs.createReadStream(file) // 1
        .pipe(zlib.createGzip()) // 2,3,4
        .pipe(response); // 5, 6, 7
}

const srv: Server = http.createServer(handleRequest);
srv.listen(poort);
console.log(`Server gestart op http://localhost:${poort}!`);
