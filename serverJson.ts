import http, {IncomingMessage, ServerResponse} from "http";

let poort = 3000;

type Persoon = { naam: string, geboortedatum: string };
type Personen = { [key: number]: Persoon };
let personen: Personen = {
    163361: {naam: "groucho", geboortedatum: "2/10/1890"},
    150217: {naam: "ingeborg", geboortedatum: "24/4/1970"},
    142742: {naam: "karl", geboortedatum: "5/5/1818"}
};

function handleRequest(request: IncomingMessage, response: ServerResponse): void {
    console.log(`URL gevraagd: ${request.url}`);
    response.writeHead(200, {"Content-Type": "application/json"});
    response.end(JSON.stringify(personen));
}

http.createServer(handleRequest).listen(3000, () => {
    console.log(`Server gestart op http://localhost:${poort}!`);
});
