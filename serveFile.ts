/** Created by Jan de Rijke */
import http, {IncomingMessage, Server, ServerResponse} from "http";
import fs, {Stats} from "fs";
import path from "path";

const poort = 3000;
const root = path.resolve("public");
function handleRequest(request: IncomingMessage, response: ServerResponse): void {
    const file = path.join(root, request.url!);
    fs.stat(file, (err, stat) => serveFile(err, stat, file, response));
}

function serveFile(err: Error | null, stat: Stats, file: string, response: ServerResponse) : void{
    if (err || !stat.isFile()) {
        response.writeHead(404, {'Content-Type': 'text/plain'});
        response.write(`Bestand ${file} niet beschikbaar`);
        response.end();
        return;
    }
    response.writeHead(200, {'Content-Type': 'text/html'});
    let reader = fs.createReadStream(file);
    reader.on('data', chunk => response.write(chunk));
    reader.on('end', () => response.end());
    reader.on('error', err => console.log(err));
}


let srv: Server = http.createServer(handleRequest);
srv.listen(poort);
console.log(`Server gestart op http://localhost:${poort}!'`);
